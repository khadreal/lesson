<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;

class StoreController extends Controller
{
    //

    public function index()
    {
    	//create a view that shows all we have in the
    	// database and edit and delete button
    }

    public function (Request $request)
    {
    	$request->validate([
    		'title' => 'required|unique:users',
    		'category' => 'required',
    		'status'	=> 'required'
    	]);

    	$data = $request->all();
    	Store::create($data);
        return redirect()->route('store.index');
    }

    public function edit($id)
    {
    	$store = Store::findOrFail($id);

    	//Add the view file to the view method
    	return view('')->with('store', $store);
    }

    //Now the test here is for you to create a 
    // Function to save the updated data
}
