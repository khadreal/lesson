<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route 
Route::get('/store', 'StoreController@index')->name('store.index');
Route::post('/store-data', 'StoreController@post')->name('store.save');
Route::get('edit/{id}', 'StoreController@edit')->name('store.edit');

//Create a post route to save the edit data